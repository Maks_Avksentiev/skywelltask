//
//  ConfigurableCell.swift
//  SkywellTask
//
//  Created by Maksim Avksentiev.
//  Copyright © 2019 Avksentiev. All rights reserved.
//

import Foundation

public protocol ConfigurableCell {

    associatedtype Item
    
    func configure(_ item: Item, at indexPath: IndexPath)
}
