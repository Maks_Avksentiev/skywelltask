//
//  ReusableCell.swift
//  SkywellTask
//
//  Created by Maksim Avksentiev.
//  Copyright © 2019 Avksentiev. All rights reserved.
//

import UIKit

public protocol ReusableView: class {
    
    static var reuseIdentifier: String { get }
}

public extension ReusableView where Self: UIView {
    
    static var reuseIdentifier: String {
        
        return String(describing: self)
    }
}
