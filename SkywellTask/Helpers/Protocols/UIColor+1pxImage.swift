//
//  UIColor+1pxImage.swift
//  SkywellTask
//
//  Created by Maksim Avksentiev on 2/7/19.
//  Copyright © 2019 Avksentiev. All rights reserved.
//

import UIKit

extension UIColor {
    
    func lineImage() -> UIImage {
        
        UIGraphicsBeginImageContext(CGSize(width: 1, height: 1))
        setFill()
        
        UIGraphicsGetCurrentContext()?.fill(CGRect(x: 0, y: 0, width: 1, height: 1))
        let image = UIGraphicsGetImageFromCurrentImageContext() ?? UIImage()
        UIGraphicsEndImageContext()
     
        return image
    }
}
