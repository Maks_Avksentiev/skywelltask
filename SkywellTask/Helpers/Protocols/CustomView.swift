//
//  CustomView.swift
//  SkywellTask
//
//  Created by Maksim Avksentiev on 2/7/19.
//  Copyright © 2019 Avksentiev. All rights reserved.
//

import UIKit

protocol CustomViewProtocol: AnyObject {
    
    var className: String {get}
    func load()
}

extension CustomViewProtocol {
    
    func nib() -> UINib {
        
        return UINib(nibName: self.className, bundle: nil)
    }
}
