//
//  String+Localization.swift
//  SkywellTask
//
//  Created by Maksim Avksentiev on 2/14/19.
//  Copyright © 2019 Avksentiev. All rights reserved.
//

import Foundation

extension String {
    
    var localized: String {
        
        return self.localizedWithComment("")
    }
    
    func localizedWithComment(_ comment: String) -> String {
        
        let toReturnDebug = NSLocalizedString(self, tableName: nil, bundle: Bundle.main, value: self, comment: comment)
        
        //        #if DEBUG
        //            if NSLocalizedString("lang", comment: "") != "en" {
        //                if self == toReturnDebug {
        //                    log.warning("String \"\(self)\" is not localized!")
        //                }
        //            }
        //        #endif
        
        return toReturnDebug
    }
}
