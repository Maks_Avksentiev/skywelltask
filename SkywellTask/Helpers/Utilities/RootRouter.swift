//
//  RootRouter.swift
//  SkywellTask
//
//  Created by Maksim Avksentiev.
//  Copyright © Avksentiev. All rights reserved.
//

import UIKit

class RootRouter {

    var rootNavigation: UINavigationController!
    
    func setRootViewController(controller: UIViewController, animatedWithOptions: UIView.AnimationOptions?) {
        
        guard let window = UIApplication.shared.keyWindow else {
            fatalError("No window in app")
        }
        
        window.rootViewController = controller
        
        if let animationOptions = animatedWithOptions, window.rootViewController != nil {
            
            UIView.transition(with: window, duration: 0.33, options: animationOptions, animations: {
                
                //TODO: Appear animations if needed
            })
        }
    }

    func loadMainAppStructure() {
        
        let controller = MainController()
        let navigationController = UINavigationController(rootViewController: controller)
        self.rootNavigation = navigationController
        self.setRootViewController(controller: navigationController, animatedWithOptions: nil)
    }
}
