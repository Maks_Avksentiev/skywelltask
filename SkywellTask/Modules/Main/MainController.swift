//
//  MainController.swift
//  SkywellTask
//
//  Created by Maksim Avksentiev on 2/7/19.
//  Copyright © 2019 Avksentiev. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import SwiftyJSON

class MainController: BaseController {

    override class var identifier: String {
        
        return "MainController"
    }
    
    @IBOutlet weak var weatherView: WeatherView!
    @IBOutlet weak var tableView: UITableView!
    
    fileprivate var dataSource: CarsDataSource?
    fileprivate let locationService = LocationService()
    fileprivate let photosService = PhotosService()
    fileprivate let networking = AlamofireNetworking()
    
    //MARK: - LifeCycle
    override func viewDidLoad() {
    
        super.viewDidLoad()

        self.configureNavigationBar()
        self.dataSource = self.loadDataSource()
        if let forecast = Forecast.load() {
            
            self.weatherView.bind(forecast: forecast)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
       
        super.viewDidAppear(animated)

        self.weatherModuleConfigure()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        
        super.viewDidDisappear(animated)
        
        self.locationService.dispose = DisposeBag()
    }
}

//MARK: - Configurator
extension MainController {
    
    fileprivate func configureNavigationBar() {
        
        self.title = "Title.CarList".localized
        self.setBarButton()
        self.customizeBackButton()
    }
    
    fileprivate func setBarButton() {
        
        let menuBtn = UIButton(type: .custom)
        menuBtn.frame = CGRect(x: 0.0, y: 0.0, width: 20, height: 20)
        menuBtn.setImage(R.image.addButton(), for: .normal)
        menuBtn.addTarget(self, action: #selector(addPressed), for: .touchUpInside)
        
        let menuBarItem = UIBarButtonItem(customView: menuBtn)
        menuBarItem.customView?.widthAnchor.constraint(equalToConstant: 24).isActive = true
        menuBarItem.customView?.heightAnchor.constraint(equalToConstant: 24).isActive = true
        menuBarItem.tintColor = UIColor.white
        self.navigationItem.rightBarButtonItem = menuBarItem
    }
    
    fileprivate func customizeBackButton() {
        
        let backBarButtton = UIBarButtonItem(title: "Back", style: .plain, target: nil, action: nil)
        self.navigationItem.backBarButtonItem = backBarButtton
        self.navigationItem.backBarButtonItem?.tintColor = UIColor.white
    }
    
    fileprivate func weatherModuleConfigure() {
        
        self.locationService.subscribe().subscribe(onNext: { [weak self] (location) in
            
            guard let strongSelf = self, let coord = location?.coordinate else {
                
                return
            }
            
            strongSelf.networking.fetchForecast(for: coord, response: { (response) -> Observable<Forecast> in
                
                guard let forecast = Forecast(json: JSON(response)) else {
                    return Observable.never()
                }
                
                return Observable.from(optional: forecast)
            }).subscribe(onNext: { [weak self] (forecast) in
                
                Forecast.save(forecast)
                
                self?.weatherView.bind(forecast: forecast)
            }).disposed(by: strongSelf.locationService.dispose)
            
        }).disposed(by: self.locationService.dispose)
    }

    fileprivate func loadDataSource() -> CarsDataSource {
        
        let dataSource = CarsDataSource(tableView: self.tableView, array: Car.getAll(in: CoreData.manager.currentThreadContext()).reversed())
        
        dataSource.tableItemSelectionHandler = { [weak self] indexPath in
            
            self?.navigationController?.pushViewController(CarController(dataSource: CarDataSource(model: self?.dataSource?.item(at: indexPath))), animated: true)
        }
        
        return dataSource
    }
}

//MARK: - Actions
extension MainController {
    
    @objc func addPressed() {
        
        let controller = CarController(dataSource: CarDataSource())
        controller.delegate = self
        self.navigationController?.pushViewController(controller, animated: true)
    }
}

//MARK: - CarCreatorDelegate
extension MainController: CarCreatorDelegate {
    
    func created(car: CarViewModel) {
        
        self.dataSource?.provider.items.insert([car], at: 0)
        self.tableView.reloadData()
    }
}
