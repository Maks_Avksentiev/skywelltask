//
//  BaseController.swift
//  SkywellTask
//
//  Created by Maksim Avksentiev on 2/7/19.
//  Copyright © 2019 Avksentiev. All rights reserved.
//

import UIKit

class BaseController: UIViewController {

    class var identifier: String {
        
        return "BaseController"
    }
    
    //MARK: - LifeCycle
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.configure()
    }
    
}

//MARK: - SControllerLoader
extension BaseController: ControllerLoader {
    
    func load() {}
    
    @objc func configure() {
        
        self.configureNavigationAppearance()
    }
}

//MARK: - Private
extension BaseController {
    
    fileprivate func configureNavigationAppearance() {
        
        self.navigationController?.navigationBar.barTintColor = UIColor.clear
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white, NSAttributedString.Key.font: UIFont.systemFont(ofSize: 28.0, weight: UIFont.Weight.medium)]
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.navigationBar.setBackgroundImage(UIColor.clear.lineImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIColor.white.lineImage()
    }
}
