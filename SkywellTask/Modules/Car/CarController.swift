//
//  CarController.swift
//  SkywellTask
//
//  Created by Maksim Avksentiev on 2/7/19.
//  Copyright © 2019 Avksentiev. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

protocol CarCreatorDelegate: class {
    
    func created(car: CarViewModel)
}

class CarController: BaseController {
    
    override class var identifier: String {
        
        return "CarController"
    }
    
    @IBOutlet weak var sliderContainer: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var pickerView: PickerView!
    @IBOutlet weak var stackView: UIStackView!
    
    var dataSource: CarDataSource!
    weak var delegate: CarCreatorDelegate?
    var dispose = DisposeBag()

    lazy var imagePicker = UIImagePickerController()
    
    fileprivate var imageSlider: SliderController!

    //MARK: - Init
    init(dataSource: CarDataSource) {
        
        super.init(nibName: CarController.identifier, bundle: Bundle.main)
        
        self.dataSource = dataSource
    }
    
    required init?(coder aDecoder: NSCoder) {
        
        super.init(coder: aDecoder)
    }
    
    //MARK: - LifeCycle
    override func viewDidAppear(_ animated: Bool) {
     
        super.viewDidAppear(animated)

        self.keyboardConfigure()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        
        super.viewDidDisappear(animated)
        
        self.dispose = DisposeBag()
    }
    
    override func viewDidLayoutSubviews() {
        
        super.viewDidLayoutSubviews()
        
        var frame = self.sliderContainer.frame
        frame.size.height -= 20
        self.imageSlider?.view.frame = frame
    }
    
    override func configure() {
        
        super.configure()
        
        self.addRightButton()
        self.initializeSlider()
        self.configurePicker()
        
        self.configureTableView()
        self.configureCollectionView()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        super.touchesBegan(touches, with: event)
        
        self.view.endEditing(true)
    }
}

//MARK: - Configure
extension CarController {
    
    fileprivate func configureCollectionView() {
        
        self.collectionView.isHidden = !(self.dataSource.state == .create)

        guard self.dataSource.state == .create else {
            
            return
        }
        
        self.collectionView.register(ImageCell.nib(), forCellWithReuseIdentifier: ImageCell.reuseIdentifier)
        
        self.collectionView.dataSource = self.dataSource
        self.collectionView.delegate = self
    }
    
    fileprivate func configureTableView() {
        
        self.tableView.register(MainCarInfoCell.nib(), forCellReuseIdentifier: MainCarInfoCell.reuseIdentifier)
        self.tableView.register(AdditionalCarInfoCell.nib(), forCellReuseIdentifier: AdditionalCarInfoCell.reuseIdentifier)
        
        self.tableView.register(TableFooterCell.nib(), forHeaderFooterViewReuseIdentifier: TableFooterCell.reuseIdentifier)
        
        self.tableView.dataSource = self.dataSource
        self.tableView.delegate = self
        
        self.tableView.allowsSelection = (self.dataSource.state == .create)
    }
    
    fileprivate func keyboardConfigure() {
        
        guard self.dataSource.state == .create else {
            
            return
        }
        
        RxKeyboard.instance
            .visibleHeight
            .drive(onNext: { [weak self] keyboardVisibleHeight in
                
                self?.tableView?.contentInset.bottom = keyboardVisibleHeight
                if keyboardVisibleHeight != 0 {
                    self?.pickerView.isHidden = true
                    self?.pickerView.alpha = 0.0
                }
            })
            .disposed(by: self.dispose)
        
        RxKeyboard.instance
            .willShowVisibleHeight
            .drive(onNext: { [weak self] keyboardVisibleHeight in
                
                self?.tableView.contentOffset.y += keyboardVisibleHeight
            })
            .disposed(by: self.dispose)
    }
    
    fileprivate func initializeSlider() {
        
        self.sliderContainer.isHidden = !(self.dataSource.state == .viewing)

        guard self.dataSource.state == .viewing, let model = self.dataSource.viewModel else {
            
            return
        }
        
        let slider = ImageSlider(images: PhotosService.getImages(for: model.id, with: model.imagesCount))
        self.imageSlider = SliderController(slider: slider)
        self.addChild(self.imageSlider)
        self.stackView.addSubview(self.imageSlider.view)
        self.imageSlider.didMove(toParent: self)
    }
    
    fileprivate func addRightButton() {
        
        guard self.dataSource.state == .viewing else {
            
            let button = UIButton(type: .custom)
            button.setTitle("Title.AddCar".localized, for: .normal)
            button.setTitleColor(UIColor.white, for: .normal)
            button.titleLabel?.font = UIFont.systemFont(ofSize: 28.0, weight: UIFont.Weight.medium)
            button.sizeToFit()
            button.addTarget(self, action: #selector(addButtonClicked(_:)), for: .touchUpInside)

            self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: button)

            return
        }
        
        self.title = "Title.CarInfo".localized
    }
    
    fileprivate func configurePicker() {
        
        self.pickerView.delegate = self
    }
}

//MARK: - Actions
extension CarController {
    
    @objc func addButtonClicked(_ sender: UIBarButtonItem) {
        
        if let model = self.dataSource.viewModel, model.isValid, let car: Car = CoreData.manager.createOrUpdate(CoreData.manager.currentThreadContext(), primary: ["name": model.name, "id": model.id], secondary: ["transmission": model.transmission, "price": model.price, "condition": model.condition, "engine": model.engine, "imagesCount": self.dataSource.images.count]) {
            
            PhotosService.save(images: self.dataSource.images, for: model.id)
            self.delegate?.created(car: CarViewModel(car: car))
            
            self.navigationController?.popViewController(animated: true)
        }
    }
}

//MARK: - PickerViewDelegate
extension CarController: PickerViewDelegate {
  
    func confirm(_ object: Any) {
        
        switch object {
        case let value as Condition:
            self.dataSource.viewModel?.condition = value.rawValue
        case let value as Transmission:
            self.dataSource.viewModel?.transmission = value.rawValue
        case let value as Double:
            self.dataSource.viewModel?.engine = value
        case let value as (String, String):
            self.dataSource.viewModel?.name = value.0
            self.dataSource.viewModel?.price = Int(value.1) ?? 0
        default:
            break
        }
        
        if let indexPath = self.tableView.indexPathForSelectedRow {
            
            self.tableView.reloadRows(at: [indexPath], with: .fade)
            self.tableView.deselectRow(at: indexPath, animated: true)
        }
    }
}

//MARK: - UITableViewDelegate
extension CarController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        switch indexPath.row {
        case 1, 2, 3:
            
            self.pickerView.selected(AdditionalParams(rawValue: indexPath.row)!)
        default:
            break
        }
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        
        return tableView.dequeueReusableHeaderFooterView(withIdentifier: TableFooterCell.reuseIdentifier) as? TableFooterCell
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        
        return 190.0
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return CGFloat.leastNormalMagnitude
    }
}

//MARK: - UICollectionViewDelegate
extension CarController: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if indexPath.row == self.dataSource.images.count {
            
            guard UIImagePickerController.isSourceTypeAvailable(.photoLibrary) else {

                return
            }
            
            self.imagePicker.sourceType = .photoLibrary
            self.imagePicker.delegate = self
            
            self.present(self.imagePicker, animated: true)
        } else {
            
            self.dataSource.images.remove(at: indexPath.row)
            collectionView.deleteItems(at: [indexPath])
        }
    }
}

//MARK: - UICollectionViewDelegateFlowLayout
extension CarController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let size = (collectionView.frame.height - 50.0) 
        return .init(width: size, height: size)
    }
}

//MARK: - UIImagePickerControllerDelegate & UINavigationControllerDelegate
extension CarController: UIImagePickerControllerDelegate & UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
     
        defer {
            picker.dismiss(animated: true)
        }
        
        guard let image = info[UIImagePickerController.InfoKey.imageURL] as? URL else {
            
            return
        }
        
        self.dataSource.images.insert(image.absoluteString, at: 0)
        self.collectionView.insertItems(at: [IndexPath(row: 0, section: 0)])
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
       
        defer {
            picker.dismiss(animated: true)
        }
    }
}
