//
//  CarStateMachine.swift
//  SkywellTask
//
//  Created by Maksim Avksentiev on 2/14/19.
//  Copyright © 2019 Avksentiev. All rights reserved.
//

import Foundation

enum CarStateMachine {
    case create
    case viewing
}
