//
//  PhotosService.swift
//  SkywellTask
//
//  Created by Maksim Avksentiev on 2/14/19.
//  Copyright © 2019 Avksentiev. All rights reserved.
//

import RxCocoa
import RxSwift
import RxPhotos
import Photos

class PhotosService {
    
    let dispose = DisposeBag()
    
    init() {
        
        self.requestAccess()
        
        try? FileManager.default.createDirectory(at: PhotosService.getDocumentsDirectory().appendingPathComponent("Images"), withIntermediateDirectories: false, attributes: nil)
    }
    
    func requestAccess() {
        
        PHPhotoLibrary.rx
            .requestAuthorization()
            .subscribe(onSuccess: { status in
                switch status {
                case .authorized:
                    break
                default:
                    break
                }
            }).disposed(by: self.dispose)
    }
    
    class func getDocumentsDirectory() -> URL {
        
        return FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
    }
    
    class func savedDirectoryPath() -> URL {

        return PhotosService.getDocumentsDirectory().appendingPathComponent("Images")
    }
    
    class func save(images: [UIImage], for id: String) {
        
        images.enumerated().forEach({
            
            let imagePath = PhotosService.savedDirectoryPath().appendingPathComponent(id + "-" + String($0.offset))
            
            if let jpegData = $0.element.jpegData(compressionQuality: 0.1) {
                try? jpegData.write(to: imagePath)
            }
        })
    }
    
    class func save(images: [String], for id: String) {
        
        images.enumerated().forEach {
            
            let path = self.savedDirectoryPath().appendingPathComponent(id + "-" + String($0.offset))
            
            if let inputPath = URL(string: $0.element)?.path {
                
                try? FileManager.default.copyItem(atPath: inputPath, toPath: path.path)
            }
        }
    }
    
    class func remove(for id: String, count: Int) {
        
        let _ = Array(0..<count).compactMap {
            try? FileManager.default.removeItem(at: self.savedDirectoryPath().appendingPathComponent(id + "-" + "\($0)"))
        }
    }

    class func mainImage(for id: String) -> UIImage? {
        
        return UIImage(contentsOfFile: self.savedDirectoryPath().appendingPathComponent(id + "-0").path) ?? R.image.placeholder()
    }
    
    class func getImages(for id: String, with count: Int) -> [UIImage] {
        
        return (count != 0 ? (Array(0..<count).compactMap {
            UIImage(contentsOfFile: self.savedDirectoryPath().appendingPathComponent(id + "-" + "\($0)").path)
        }) : ([R.image.placeholder()!]))
    }
    
    class func get(for id: String, with count: Int) -> [String] {
    
        return (count != 0 ? (Array(0..<count).compactMap {
            self.savedDirectoryPath().appendingPathComponent(id + "-" + "\($0)").path //absoluteString
        }) : ([]))
    }
}
