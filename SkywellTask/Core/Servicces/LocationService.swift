//
//  LocationService.swift
//  SkywellTask
//
//  Created by Maksim Avksentiev on 2/13/19.
//  Copyright © 2019 Avksentiev. All rights reserved.
//

import RxCocoa
import RxSwift
import RxCoreLocation
import CoreLocation

class LocationService {
    
    let manager = CLLocationManager()
    var dispose = DisposeBag()

    //MARK: - Init
    init() {
        
        self.requestAccess()
    }
    
    //MARK: - Main
    func requestAccess() {
        
        self.manager.rx.didChangeAuthorization.subscribe(onNext: { (manager, status) in
            
            switch status {
            case .authorizedWhenInUse:
                self.manager.startUpdatingLocation()
            default:
                self.manager.requestWhenInUseAuthorization()
            }
            
        }).disposed(by: self.dispose)
    }
    
    func subscribe() -> Observable<CLLocation?> {
        
        return self.manager.rx.location
    }
}

