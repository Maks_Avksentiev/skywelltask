//
//  CollectionDataSource.swift
//  SkywellTask
//
//  Created by Maksim Avksentiev on 2/8/19.
//  Copyright © 2019 Avksentiev. All rights reserved.
//

import UIKit

open class CollectionDataSource<Provider: DataProvider, Cell: ConfigurableCell & UICollectionViewCell>: NSObject, UICollectionViewDataSource, UICollectionViewDelegate where Cell: NibLoadable, Provider.T == Cell.Item {
    
    public var tableItemSelectionHandler: ItemSelectionHandlerType?
    
    let provider: Provider
    let collectionView: UICollectionView
    
    // MARK: - Initialize
    init(collectionView: UICollectionView, provider: Provider) {
        
        self.collectionView = collectionView
        self.provider = provider
        
        super.init()
        
        self.setUp()
    }
    
    //MARK: - Private
    fileprivate func setUp() {
        
        DispatchQueue.main.async {
            
            self.collectionView.register(Cell.nib(), forCellWithReuseIdentifier: Cell.reuseIdentifier)
            self.collectionView.dataSource = self
            self.collectionView.delegate = self
        }
    }
    
    // MARK: - UICollectionViewDataSource
    public func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        return self.provider.numberOfSections()
    }
    
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return self.provider.numberOfItems(in: section)
    }
    
    open func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Cell.reuseIdentifier, for: indexPath) as? Cell else {
            
            return UICollectionViewCell()
        }
        
        if let item = self.provider.item(at: indexPath) {
            
            cell.configure(item, at: indexPath)
        }
        
        return cell
    }
    
    // MARK: - UICollectionViewDelegate
    public func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        self.tableItemSelectionHandler?(indexPath)
    }
}
