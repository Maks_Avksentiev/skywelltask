//
//  TableDataSource.swift
//  SkywellTask
//
//  Created by Maksim Avksentiev.
//  Copyright © 2019 Avksentiev. All rights reserved.
//

import UIKit

open class TableDataSource<Provider: DataProvider, Cell: ConfigurableCell & UITableViewCell>: NSObject, UITableViewDataSource, UITableViewDelegate where Cell: NibLoadable, Provider.T == Cell.Item {
    
    public var tableItemSelectionHandler: ItemSelectionHandlerType?
    
    let provider: Provider
    let tableView: UITableView
    
    // MARK: - Initialize
    init(tableView: UITableView, provider: Provider) {
        
        self.tableView = tableView
        self.provider = provider
        
        super.init()
        
        self.setUp()
    }
    
    //MARK: - Private
    fileprivate func setUp() {
        
        self.tableView.register(Cell.nib(), forCellReuseIdentifier: Cell.reuseIdentifier)
        self.tableView.dataSource = self
        self.tableView.delegate = self
    }
    
    // MARK: - UITableViewDataSource
    public func numberOfSections(in tableView: UITableView) -> Int {
        
        return self.provider.numberOfSections()
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.provider.numberOfItems(in: section)
    }
    
    open func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: Cell.reuseIdentifier, for: indexPath) as? Cell else {
            
            return UITableViewCell()
        }
        
        if let item = self.provider.item(at: indexPath) {
            
            cell.configure(item, at: indexPath)
        }
        
        return cell
    }
    
    // MARK: - UITableViewDelegate
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        self.tableItemSelectionHandler?(indexPath)
    }
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 66.0
    }
    
    public func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        
        if editingStyle == .delete {
            
            if let item = self.provider.item(at: indexPath) as? CarViewModel {
                
                let _: Car? = CoreData.manager.delete(CoreData.manager.currentThreadContext(), parametrs: ["id": item.id, "name": item.name])
                self.provider.removeItem(at: indexPath)
                tableView.deleteRows(at: [indexPath], with: .left)
                
                PhotosService.remove(for: item.id, count: item.imagesCount)
            }
            
           
            
        }
    }
    
}
