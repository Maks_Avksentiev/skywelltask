//
//  CarsDataSource.swift
//  SkywellTask
//
//  Created by Maksim Avksentiev on 2/8/19.
//  Copyright © 2019 Avksentiev. All rights reserved.
//

import Foundation

class CarsDataSource: ArrayDataSource<CarViewModel, CarCell> {}
