//
//  CarDataSource.swift
//  SkywellTask
//
//  Created by Maksim Avksentiev on 2/14/19.
//  Copyright © 2019 Avksentiev. All rights reserved.
//

import UIKit

//class CarPresenter: NSObject {
//
//}

class CarDataSource: NSObject {
    
    var viewModel: CarViewModel?
    var state: CarStateMachine!
    
    var images = [String]()
    
//    var presenter = CarPresenter()
    
    //MARK: - Init
    init(model: CarViewModel? = nil) {
        
        self.viewModel = model ?? CarViewModel(id: String(format: "%.2f", Date().timeIntervalSince1970), name: "", price: 0, engine: 1.0, transmission: 0, condition: 0, imagesCount: 0)
        self.state = (model == nil ? .create : .viewing)
        
        if let model = self.viewModel {
            
            self.images = PhotosService.get(for: model.id, with: model.imagesCount)
        }
    }
}

//MARK: - PickerViewDelegate
extension CarDataSource: PickerViewDelegate {
    
    func confirm(_ object: Any) {
        
        switch object {
        case let value as Condition:
            self.viewModel?.condition = value.rawValue
        case let value as Transmission:
            self.viewModel?.transmission = value.rawValue
        case let value as Double:
            self.viewModel?.engine = value
        case let value as (String, String):
            self.viewModel?.name = value.0
            self.viewModel?.price = Int(value.1) ?? 0
        default:
            break
        }
        
        
//        if let indexPath = self.tableView.indexPathForSelectedRow {
//
//            self.tableView.reloadRows(at: [indexPath], with: .fade)
//            self.tableView.deselectRow(at: indexPath, animated: true)
//        }
    }
}

//MARK: - UITableViewDataSource
extension CarDataSource: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.row {
        case 0:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: MainCarInfoCell.reuseIdentifier, for: indexPath) as? MainCarInfoCell else {
                
                return UITableViewCell()
            }
            
            cell.delegate = self
            
            cell.configure(with: self.state)
            cell.set(title: self.viewModel?.name ?? "", price: self.viewModel?.price ?? 0)
            
            return cell
        case 1, 2, 3:
            
            guard let cell = tableView.dequeueReusableCell(withIdentifier: AdditionalCarInfoCell.reuseIdentifier, for: indexPath) as? AdditionalCarInfoCell, let type = AdditionalParams(rawValue: indexPath.row) else {
                
                return UITableViewCell()
            }
            
            cell.set(title: type.title + ":")
            cell.set(value: " " + type.getValue(from: self.viewModel))
            
            return cell
        default:
            return UITableViewCell()
        }
    }
}

//MARK: - UICollectionViewDataSource
extension CarDataSource: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return self.images.count + 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ImageCell.reuseIdentifier, for: indexPath) as? ImageCell else {
            
            return UICollectionViewCell()
        }
        
        var image = R.image.addButton()
        
        if indexPath.row != self.images.count, let url = URL(string: self.images[indexPath.row]), let imageData = try? Data(contentsOf: url) {
            
            image = UIImage(data: imageData)
        }
        
        cell.imageView.contentMode = (indexPath.row != self.images.count ? .scaleAspectFill : .center)
        cell.imageView.image = image
        
        return cell
    }
}
