//
//  Car.swift
//  SkywellTask
//
//  Created by Maksim Avksentiev on 2/14/19.
//  Copyright © 2019 Avksentiev. All rights reserved.
//

import Foundation
import CoreData

extension Car {
    
    class func getAll(in context: NSManagedObjectContext) -> [CarViewModel] {
        
        let array: [Car] = CoreData.manager.read(context)
        
        return array.compactMap{CarViewModel(car: $0)}
    }
}
