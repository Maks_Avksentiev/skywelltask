//
//  AdditionalParams.swift
//  SkywellTask
//
//  Created by Maksim Avksentiev on 2/8/19.
//  Copyright © 2019 Avksentiev. All rights reserved.
//

import Foundation

enum AdditionalParams: Int {
    
    case unknown = 0
    case engine
    case transmission
    case condition
    
    var title: String {
        
        switch self {
        case .engine:
            return "Engine"
        case .transmission:
            return "Transmission"
        case .condition:
            return "Condition"
        case .unknown:
            return "Unknown"
        }
    }
    
    var allValues: [Any] {
        
        switch self {
        case .engine:
            return Array(stride(from: 1.5, through: 5.0, by: 0.1))
        case .transmission:
            return Transmission.allCases
        case .condition:
            return Condition.allCases
        case .unknown:
            return [Any]()
        }
    }
    
    //MARK: - Main
    func getValue(from model: CarViewModel?) -> String {
        
        guard let model = model else {
            
            return ""
        }
        
        switch self {
        case .engine:
            return String(format: "%.1f", model.engine)
        case .transmission:
            return Transmission(rawValue: model.transmission)?.smallDescription ?? ""
        case .condition:
            return Condition(rawValue: model.condition)?.description ?? ""
        case .unknown:
            return ""
        }
    }
    
}
