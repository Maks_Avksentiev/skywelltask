//
//  CarViewModel.swift
//  SkywellTask
//
//  Created by Maksim Avksentiev on 2/8/19.
//  Copyright © 2019 Avksentiev. All rights reserved.
//

import Foundation

struct CarViewModel {
    
    var id: String
    var name: String
    var price: Int
    var engine: Double
    var transmission: Int
    var condition: Int
    var imagesCount: Int
    
    var isValid: Bool {
        
        return self.id != "0" && self.name != "" && self.price>0
    }
}

extension CarViewModel {
    
    init(car: Car) {
        
        self.id = car.id ?? ""
        self.name = car.name ?? ""
        self.condition = Int(car.condition)
        self.engine = car.engine
        self.transmission = Int(car.transmission)
        self.price = Int(car.price)
        self.imagesCount = Int(car.imagesCount)
    }
}
