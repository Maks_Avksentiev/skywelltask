//
//  Transmission.swift
//  SkywellTask
//
//  Created by Maksim Avksentiev on 2/8/19.
//  Copyright © 2019 Avksentiev. All rights reserved.
//

import Foundation

enum Transmission: Int {
    
    case at = 0
    case mt
    case am
    case cvt
    
    var description: String {
        
        switch self {
        case .at:
            return "Automatic Transmission"
        case .mt:
            return "Manual Transmission"
        case .am:
            return "Automated Manual Transmission"
        case .cvt:
            return "Continuously Variable Transmission"
        }
    }
    
    var smallDescription: String {
        
        switch self {
        case .at:
            return "AT"
        case .mt:
            return "MT"
        case .am:
            return "AM"
        case .cvt:
            return "CVT"
        }
    }
}

//MARK: - CaseIterable
extension Transmission: CaseIterable {}
