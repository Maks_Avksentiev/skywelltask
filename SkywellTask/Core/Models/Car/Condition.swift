//
//  Condition.swift
//  SkywellTask
//
//  Created by Maksim Avksentiev on 2/8/19.
//  Copyright © 2019 Avksentiev. All rights reserved.
//

import Foundation

enum Condition: Int {
    
    case bad = 0
    case normaly
    case good
    case best
    
    var description: String {
        
        switch self {
        case .bad:
            return "Bad"
        case .normaly:
            return "Normaly"
        case .good:
            return "Good"
        case .best:
            return "Best"
        }
    }
}

//MARK: - CaseIterable
extension Condition: CaseIterable {}
