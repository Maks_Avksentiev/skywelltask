//
//  Forecast.swift
//  SkywellTask
//
//  Created by Maksim Avksentiev on 2/13/19.
//  Copyright © 2019 Avksentiev. All rights reserved.
//

import Foundation
import SwiftyJSON

final class Forecast {

    static let key = "ForecastSaveKey"
    
    var name: String
    var country: String
    var temp: Double
    var latitude: Double
    var longitude: Double
    var weatherDescription: String
    var icon: String
    
    //MARK: - Init
    init?(json: JSON) {
    
        guard
            let name = json["name"].string,
            let country = json["sys"]["country"].string,
            let temp = json["main"]["temp"].double,
            let weather = json["weather"].array?.first?["description"].string,
            let icon = json["weather"].array?.first?["icon"].string,
            let coordLat = json["coord"]["lat"].double,
            let coordLon = json["coord"]["lon"].double
            else {
                return nil
        }
        
        self.name = name
        self.country = country
        self.temp = temp
        self.weatherDescription = weather
        self.icon = icon
        self.latitude = coordLat
        self.longitude = coordLon
    }
    
    class func load() -> Forecast? {

        guard let data = UserDefaults.standard.value(forKey: Forecast.key) as? Data, let forecast = try? PropertyListDecoder().decode(Forecast.self, from: data) else {
            return nil
        }
        
        return forecast
    }
    
    class func save(_ forecast: Forecast) {
        
        UserDefaults.standard.set(try? PropertyListEncoder().encode(forecast), forKey: Forecast.key)
    }
}

//MARK: - Codable
extension Forecast: Codable {}
