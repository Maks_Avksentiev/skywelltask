//
//  AlamofireNetworking.swift
//  SkywellTask
//
//  Created by Maksim Avksentiev on 2/13/19.
//  Copyright © 2019 Avksentiev. All rights reserved.
//

import Foundation
import Alamofire
import RxAlamofire
import RxSwift
import CoreLocation

struct AlamofireNetworking: NetworkingProtocol {
    
    func fetchForecast(for coords: CLLocationCoordinate2D, response: @escaping (Any) -> Observable<Forecast>) -> Observable<Forecast> {
    
        let params: [String: String] = [
            "lat": String(coords.latitude),
            "lon": String(coords.longitude),
//            "type": "like",
            "sort": "population",
            "units": "metric",
            "appid": API.apiKey]
        
        return RxAlamofire
            .json(.get, Endpoints.forecast.fetch.url, parameters: params)
            .flatMap(response)
    }
    
    func fetchImage(forImageId id: String) -> Observable<Data> {
        
        return RxAlamofire
            .request(.get, Endpoints.Image.fetch.url + id + ".png")
            .data()
    }
}
