//
//  Endpoints.swift
//  SkywellTask
//
//  Created by Maksim Avksentiev on 2/13/19.
//  Copyright © 2019 Avksentiev. All rights reserved.
//

import Foundation

struct API {
    
    static let baseUrl = "http://api.openweathermap.org/"
    static let apiKey = "6fe95bb8e41b80a33bdabf14d71b0608"
}

protocol Endpoint {
    
    var path: String { get }
    var url: String { get }
}

enum Endpoints {
    
    enum forecast: Endpoint {
        
        case fetch
        
        public var path: String {
            switch self {
            case .fetch: return "data/2.5/weather"
            }
        }
        
        public var url: String {
            switch self {
            case .fetch: return "\(API.baseUrl)\(path)"
            }
        }
    }
    
    enum Image: Endpoint {
        case fetch
        
        public var path: String {
            switch self {
            case .fetch: return "/img/w"
            }
        }
        
        public var url: String {
            switch self {
            case .fetch: return "\(API.baseUrl)\(path)/"
            }
        }
    }
}
