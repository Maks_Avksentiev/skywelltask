//
//  NetworkingProtocol.swift
//  SkywellTask
//
//  Created by Maksim Avksentiev on 2/13/19.
//  Copyright © 2019 Avksentiev. All rights reserved.
//

import Foundation
import RxSwift
import CoreLocation

protocol NetworkingProtocol {

    func fetchForecast(for coords: CLLocationCoordinate2D, response: @escaping (Any) -> Observable<Forecast>) -> Observable<Forecast>
    
    func fetchImage(forImageId id: String) -> Observable<Data>
}
