//
//  PickerView.swift
//  SkywellTask
//
//  Created by Maksim Avksentiev on 2/13/19.
//  Copyright © 2019 Avksentiev. All rights reserved.
//

import UIKit

protocol PickerViewDelegate: class {
    
    func confirm(_ object: Any)
}

class PickerView: SBaseView {

    override var className: String {
        return "PickerView"
    }
    
    @IBOutlet weak var picker: UIPickerView!
    @IBOutlet weak var confirmButton: UIButton!
    
    weak var delegate: PickerViewDelegate?

    fileprivate var data = [Any]()
    var type = AdditionalParams.unknown
    
    //MARK: - LifeCycle
    override func awakeFromNib() {
        
        super.awakeFromNib()
        
        self.configuration()
    }
}

//MARK: - Configuration
extension PickerView {
    
    fileprivate func configuration() {
        
        self.picker.dataSource = self
        self.picker.delegate = self
    }
    
    func configure(array: [Any], type: AdditionalParams) {
        
        self.type = type
        self.data = array
        self.picker.reloadAllComponents()
    }
}

//MARK: - UIPickerViewDataSource
extension PickerView: UIPickerViewDataSource {

    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        return self.data.count
    }
}

//MARK: - UIPickerViewDelegate
extension PickerView: UIPickerViewDelegate {
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        switch self.data {
        case let (array as [Condition]) as Any:
            return array[row].description
        case let (array as [Transmission]) as Any:
            return array[row].smallDescription
        case let (array as [Double]) as Any:
            return String(format: "%.1f", array[row])
        default:
            return ""
        }
    }
}

//MARK: - Actions
extension PickerView {
    
    @IBAction func confirm() {
        
        let index = self.picker.selectedRow(inComponent: 0)
        if index != -1 {
            
            self.delegate?.confirm(self.data[index])
            self.selected(self.type)
        }
    }
    
    func selected(_ type: AdditionalParams) {
        
        self.alpha = self.isHidden ? 0.0 : 1.0
        
        if self.type == type {
            
            UIView.animate(withDuration: 0.25, animations: {
                self.alpha = 0.0
            }) { (success) in
                if success {
                    self.isHidden = true
                }
            }
            
            self.type = .unknown
        } else {
            
            self.configure(array: type.allValues, type: type)
            
            self.isHidden = false
            UIView.animate(withDuration: 0.25, animations: {
                self.alpha = 1.0
            }) { (success) in
                if success {
                    
                }
            }
        }
    }
    
}
