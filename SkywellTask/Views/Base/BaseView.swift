//
//  BaseView.swift
//  SkywellTask
//
//  Created by Maksim Avksentiev on 2/7/19.
//  Copyright © 2019 Avksentiev. All rights reserved.
//

import UIKit

class SBaseView: UIView {
    
    var className: String {
        return "SBaseView"
    }
    
    @IBOutlet weak var containerView: UIView!
    
    //MARK: - LifeCycle
    override func awakeFromNib() {
        
        super.awakeFromNib()
        
        self.configure()
    }
    
    override init(frame: CGRect) {
        
        super.init(frame: frame)
        
        self.load()
    }
    
    required init?(coder aDecoder: NSCoder) {
        
        super.init(coder: aDecoder)
        
        self.load()
    }

}

//MARK: - Main
extension SBaseView: CustomViewProtocol {
    
    func configure() {
        
        self.addSubview(self.containerView)
        self.containerView.frame = self.bounds
        self.containerView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    }
    
    func load() {
        
        Bundle.main.loadNibNamed(self.className, owner: self, options: nil)
    }
}
