//
//  CarCell.swift
//  SkywellTask
//
//  Created by Maksim Avksentiev on 2/7/19.
//  Copyright © 2019 Avksentiev. All rights reserved.
//

import UIKit

class CarCell: TableCellFromNib {

    @IBOutlet weak var iconView: UIImageView!
    @IBOutlet weak var carTitle: UILabel!
    @IBOutlet weak var priceTitle: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    
    //MARK: - LifeCycle
    override func prepareForReuse() {
        
        super.prepareForReuse()
        
        self.iconView.image = nil
    }
    
    override func awakeFromNib() {
        
        super.awakeFromNib()
        
        self.carTitle.text = "Car"
        self.priceTitle.text = "Price"
    }
}

//MARK: - ConfigurableCell
extension CarCell: ConfigurableCell {
    
    typealias Item = CarViewModel
    
    func configure(_ item: CarViewModel, at indexPath: IndexPath) {
        
        self.nameLabel.text = item.name
        self.priceLabel.text = "\(item.price)$"
        self.iconView.image = PhotosService.mainImage(for: item.id)
    }
}
