//
//  TableCellFromNib.swift
//  SkywellTask
//
//  Created by Maksim Avksentiev.
//  Copyright © 2019 Avksentiev. All rights reserved.
//

import UIKit

class TableCellFromNib: UITableViewCell {}

extension TableCellFromNib: NibLoadable {}

//MARK: - UITableViewCell+ReusableView
extension UITableViewCell: ReusableView {}
