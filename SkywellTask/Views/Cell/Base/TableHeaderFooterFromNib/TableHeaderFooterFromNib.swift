//
//  TableHeaderFooterFromNib.swift
//  SkywellTask
//
//  Created by Maksim Avksentiev.
//  Copyright © 2019 Avksentiev. All rights reserved.
//

import UIKit

class TableHeaderFooterFromNib: UITableViewHeaderFooterView {}

extension TableHeaderFooterFromNib: NibLoadable {}

//MARK: - UITableViewHeaderFooterView+ReusableView
extension UITableViewHeaderFooterView: ReusableView {}
