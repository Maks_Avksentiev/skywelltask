//
//  CollectionCellFromNib.swift
//  SkywellTask
//
//  Created by Maksim Avksentiev.
//  Copyright © 2019 Avksentiev. All rights reserved.
//

import UIKit

class CollectionCellFromNib: UICollectionViewCell {}

extension CollectionCellFromNib: NibLoadable {}

//MARK: - UICollectionReusableView+ReusableView
extension UICollectionReusableView: ReusableView {}
