//
//  AdditionalCarInfoCell.swift
//  SkywellTask
//
//  Created by Maksim Avksentiev on 2/8/19.
//  Copyright © 2019 Avksentiev. All rights reserved.
//

import UIKit

class AdditionalCarInfoCell: TableCellFromNib {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var valueLabel: UILabel!
    
    func set(title: String) {
        
        self.titleLabel.text = title
    }
    
    func set(value: String) {
        
        self.valueLabel.text = value
    }
}
