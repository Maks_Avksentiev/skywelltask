//
//  ImageCell.swift
//  SkywellTask
//
//  Created by Maksim Avksentiev on 2/14/19.
//  Copyright © 2019 Avksentiev. All rights reserved.
//

import UIKit

class ImageCell: CollectionCellFromNib {
    
    @IBOutlet weak var imageView: UIImageView!
    
}
