//
//  TableFooterCell.swift
//  SkywellTask
//
//  Created by Maksim Avksentiev on 2/8/19.
//  Copyright © 2019 Avksentiev. All rights reserved.
//

import UIKit

class TableFooterCell: TableHeaderFooterFromNib {
    
    var className: String {
        
        return "TableFooterCell"
    }
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var contentLabel: UITextView!
    
    //MARK: - Init
    init() {
        
        super.init(reuseIdentifier: "TableFooterCell")
        self.load()
    }
    
    required init?(coder aDecoder: NSCoder) {
        
        super.init(coder: aDecoder)
    }
    
    //MARK: - Main
    func set(title: String, content: String) {
        
        self.titleLabel.text = title
        self.contentLabel.text = content
    }
}

//MARK: - CustomViewProtocol
extension TableFooterCell: CustomViewProtocol {
   
    func load() {
        
        Bundle.main.loadNibNamed(self.className, owner: self, options: nil)
    }
}
