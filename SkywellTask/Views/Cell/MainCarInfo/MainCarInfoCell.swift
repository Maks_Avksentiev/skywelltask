//
//  MainCarInfoCell.swift
//  SkywellTask
//
//  Created by Maksim Avksentiev on 2/8/19.
//  Copyright © 2019 Avksentiev. All rights reserved.
//

import UIKit

class MainCarInfoCell: TableCellFromNib {
    
    @IBOutlet weak var carTitle: UILabel!
    @IBOutlet weak var priceTitle: UILabel!
    @IBOutlet weak var divider: UIView!
    @IBOutlet weak var carField: UITextField!
    @IBOutlet weak var priceField: UITextField!
    @IBOutlet weak var mainStack: UIStackView!
    
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    @IBOutlet weak var dividerBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var dividerTopConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var carStack: UIStackView!
    @IBOutlet weak var priceStack: UIStackView!
    
    weak var delegate: PickerViewDelegate?
    
    //MARK: - LifeCycle
    override func awakeFromNib() {
        
        super.awakeFromNib()
        
        self.carTitle.text = "Title.Car".localized
        self.priceTitle.text = "Title.Price".localized
        
        self.carField.delegate = self
        self.priceField.delegate = self
    }
    
}

//MARK: - Main
extension MainCarInfoCell {
    
    func set(title: String, price: Int) {
        
        self.carField.text = title
        self.priceField.text = "\(price)$"
    }
}

//MARK: - Configuration
extension MainCarInfoCell {
    
    func configure(with state: CarStateMachine) {
        
        guard state != .viewing else {
            
            self.carField.isUserInteractionEnabled = false
            self.priceField.isUserInteractionEnabled = false
            return
        }
        
        self.heightConstraint.constant = 80
        self.mainStack.spacing = 4.0
        self.mainStack.axis = .vertical
        self.mainStack.alignment = .fill
        self.mainStack.distribution = .fill
        
        self.carStack.alignment = .leading
        self.carStack.distribution = .fillProportionally
        self.priceStack.alignment = .leading
        self.priceStack.distribution = .fillProportionally
        
        self.divider.constraints.forEach({
            self.divider.removeConstraint($0)
        })
        
        self.dividerBottomConstraint?.isActive = false
        self.dividerTopConstraint?.isActive = false
        
        self.divider.addConstraints([NSLayoutConstraint(item: self.divider, attribute: .height, relatedBy: .equal, toItem: self.divider, attribute: .height, multiplier: 1, constant: 0.5)])
    }
}

//MARK: - UITextFieldDelegate
extension MainCarInfoCell: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
  
        if let index = textField.text?.firstIndex(of: "$"), textField != self.carField {
            
            textField.text?.remove(at: index)
            textField.selectedTextRange = textField.textRange(from: textField.beginningOfDocument, to: textField.endOfDocument)   
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextField.DidEndEditingReason) {
        
        if textField != self.carField {
            
            textField.text?.append("\(textField.text == "" ? "0" : "")$")
        }
        
        var priceText = self.priceField.text
        priceText?.removeLast()
        self.delegate?.confirm((self.carField.text ?? "", priceText ?? "0"))
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        _ = textField == self.carField ? (self.priceField.becomeFirstResponder()) : (textField.resignFirstResponder())
        
        return true
    }
}
