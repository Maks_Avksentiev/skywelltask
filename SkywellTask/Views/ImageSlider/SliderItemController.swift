//
//  SliderItemController.swift
//  SkywellTask
//
//  Created by Maksim Avksentiev on 2/8/19.
//  Copyright © 2019 Avksentiev. All rights reserved.
//

import UIKit

@objc protocol SliderItemDelegate {
    
    func itemPressedAtIndex(_ index: Int)
}

class SliderItemController: UIViewController {
   
    fileprivate var imageView: UIImageView!
    fileprivate var button: UIButton!
    fileprivate var lblDescription: UILabel?
    fileprivate var labelContainer: UIView?
    
    var index = 0
    var image: UIImage?
    var desc: String?
    
    fileprivate var slider: ImageSlider!
    
    weak var delegate: SliderItemDelegate?
    
    //MARK: -Init
    init(slider: ImageSlider) {
        
        super.init(nibName: nil, bundle: nil)
        
        self.slider = slider
    }
    
    required init?(coder aDecoder: NSCoder) {
        
        super.init(coder: aDecoder)
    }
    
    //MARK: - LifeCycle
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.configure()
    }
    
    override func viewDidLayoutSubviews() {
        
        super.viewDidLayoutSubviews()
        
        self.imageView.frame = self.view.frame
        self.button.frame = self.imageView.frame
        
        let lblHeight = self.view.frame.size.height / 2
        let lblY = self.view.frame.origin.y + lblHeight
        self.lblDescription?.frame = CGRect(x: self.view.frame.origin.x + 10, y: lblY, width: self.view.frame.size.width - 10, height: lblHeight)
        self.labelContainer?.frame = CGRect(x: self.view.frame.origin.x, y: lblY, width: self.view.frame.size.width, height: lblHeight)
    }
    
}

//MARK: - Configurations
extension SliderItemController {
    
    fileprivate func configure() {
        
        if let imageview = self.slider.customImageView {
            
            self.imageView = imageview
        } else {
            
            self.imageView = UIImageView()
            self.imageView.contentMode = .scaleAspectFill
        }
        
        self.imageView.tintColor = UIColor.clear
        
        self.button = UIButton()
        self.button.addTarget(self, action: #selector(SliderItemController.pressed(_:)), for: .touchUpInside)
        
        self.view.addSubview(self.imageView)
        self.view.addSubview(self.button)

        if self.slider.sliderDescriptions.count > 0 {
            
            self.lblDescription = UILabel()
            self.lblDescription?.numberOfLines = self.slider.numberOfLinesInDescription
            self.lblDescription?.textColor = self.slider.descriptionColor
            self.lblDescription?.font = self.slider.descriptionFont

            self.labelContainer = UIView()
            self.labelContainer?.backgroundColor = self.slider.descriptionBackgroundColor
            self.labelContainer?.alpha = self.slider.descriptionBackgroundAlpha

            self.view.addSubview(self.labelContainer!)
            self.view.addSubview(self.lblDescription!)
        }
        
        self.applyConfig()
    }
    
    fileprivate func applyConfig() {
        
        let defaultImage: UIImage? = nil
        
        self.imageView.image = self.image ?? defaultImage
        
        self.imageView.contentMode = (self.image == R.image.placeholder() ? .center : .scaleAspectFill)
            
        
        if let description = self.desc {
            self.lblDescription?.text = description
        } else {
            self.labelContainer?.isHidden = true
        }
    }
}

//MARK: - Actions
extension SliderItemController {
    
    @objc internal func pressed(_ sender:UIButton) {
        
        self.delegate?.itemPressedAtIndex(self.index)
    }
}
