//
//  ImageSlider.swift
//  SkywellTask
//
//  Created by Maksim Avksentiev on 2/8/19.
//  Copyright © 2019 Avksentiev. All rights reserved.
//

import UIKit

open class ImageSlider: NSObject {
    
    open var sliderImages = [UIImage]()
    open var sliderDescriptions = [String]()
    open var descriptionColor = UIColor.white
    open var descriptionBackgroundAlpha = CGFloat(0.3)
    open var descriptionBackgroundColor = UIColor.black
    open var descriptionFont = UIFont.systemFont(ofSize: 15)
    open var numberOfLinesInDescription = 2
   
    open var customImageView: UIImageView?
    open var showPageIndicator = true
    open var userInteractionEnabled = true
    open var transitionStyle = UIPageViewController.TransitionStyle.scroll
    open var slidingOrientation = UIPageViewController.NavigationOrientation.horizontal
    open var sliderNavigationDirection = UIPageViewController.NavigationDirection.forward
    
    //MARK: - Init
    public init(images: [UIImage]) {
        
        self.sliderImages = images
    }
}
