//
//  SliderController.swift
//  SkywellTask
//
//  Created by Maksim Avksentiev on 2/8/19.
//  Copyright © 2019 Avksentiev. All rights reserved.
//

import UIKit

@objc public protocol SliderDelegate {
    
    func itemPressedAtIndex(index: Int)
}

class SliderController: UIViewController {
    
    fileprivate var pageControl: UIPageControl!
    fileprivate var pageController: UIPageViewController!
    fileprivate var currentIndex: Int = 0 {
        didSet {
            self.pageControl.currentPage = self.currentIndex
        }
    }
    
    fileprivate var slider: ImageSlider!
   
    open weak var delegate: SliderDelegate?

    //MARK: - Init
    public init(slider: ImageSlider) {
        
        super.init(nibName: nil, bundle: nil)

        self.slider = slider
    }
    
    required public init?(coder aDecoder: NSCoder) {
        
        super.init(coder: aDecoder)
    }
    
    //MARK: - LifeCycle
    override open func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.pageControl = UIPageControl()
        if self.slider.showPageIndicator {
            self.view.addSubview(self.pageControl)
        }
        self.initializePager()
//        self.pageController.view.backgroundColor = UIColor.blue
    }
    
    open override func viewDidLayoutSubviews() {
        
        super.viewDidLayoutSubviews()
        
        self.pageControl.center = CGPoint(x: UIScreen.main.bounds.size.width / 2, y: self.view.frame.size.height + 10)
    }
    
}

//MARK: - Private
extension SliderController {
    
    fileprivate func initializePager() {
        
        self.pageControl.numberOfPages = self.slider.sliderImages.count
        self.pageControl.currentPage = 0
        
        self.pageController = UIPageViewController(transitionStyle: self.slider.transitionStyle, navigationOrientation: self.slider.slidingOrientation, options: nil)
        self.pageController.dataSource = self
        self.pageController.delegate = self
        
        if !self.slider.userInteractionEnabled {
            self.pageController.view.subviews.forEach { ($0 as? UIScrollView)?.isScrollEnabled = false }
        }
        
        self.pageController.setViewControllers([self.contentViewController(atIndex: currentIndex) ?? UIViewController()], direction: .forward, animated: false, completion: nil)
        
        self.view.addSubview(self.pageController.view)
        self.view.bringSubviewToFront(self.pageControl)
        self.pageController.didMove(toParent: self)
        
        self.pageController.view.subviews.forEach {
            ($0 as? UIScrollView)?.delegate = self
        }
        
        self.slider.userInteractionEnabled = !(self.slider.sliderImages.count == 1)
        self.pageController.view.isUserInteractionEnabled = !(self.slider.sliderImages.count == 1)
        self.pageControl.isHidden = self.slider.sliderImages.count == 1
    }
    
    fileprivate func setConstraints() {

        self.view.addConstraints([
            NSLayoutConstraint(item: self.pageController.view, attribute: .top, relatedBy: .equal, toItem: self.view, attribute: .top, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: self.pageController.view, attribute: .bottom, relatedBy: .equal, toItem: self.view, attribute: .bottom, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: self.pageController.view, attribute: .left, relatedBy: .equal, toItem: self.view, attribute: .left, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: self.pageController.view, attribute: .right, relatedBy: .equal, toItem: self.view, attribute: .right, multiplier: 1, constant: 0)])
    }
    
    fileprivate func contentViewController(atIndex index: Int) -> SliderItemController! {
        
        if self.slider.sliderImages.count == 0 || index >= self.slider.sliderImages.count {
            
            self.pageControl.isHidden = true
            
            return nil
        }
        
        self.pageControl.isHidden = false
        let contentvc = SliderItemController(slider: self.slider)
        
        if self.slider.sliderImages.count > index {
            contentvc.image = self.slider.sliderImages[index]
        }
        
        if self.slider.sliderDescriptions.count > index {
            contentvc.desc = self.slider.sliderDescriptions[index]
        }
        
        contentvc.index = index
        contentvc.delegate = self
        
        return contentvc
    }
}

//MARK: - SliderItemDelegate
extension SliderController: SliderItemDelegate {
    
    func itemPressedAtIndex(_ index: Int) {
        
        self.delegate?.itemPressedAtIndex(index: index)
    }
}

//MARK: - UIPageViewControllerDataSource
extension SliderController: UIPageViewControllerDataSource {

    public func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        
        guard let index = (viewController as? SliderItemController)?.index, index != self.slider.sliderImages.count - 1 else {
            
            return self.contentViewController(atIndex: 0)
        }
        
        return self.contentViewController(atIndex: index + 1)
    }
    
    public func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
      
        guard let index = (viewController as? SliderItemController)?.index, index != 0 else {
            
            return self.contentViewController(atIndex: self.slider.sliderImages.count - 1)
        }

        return self.contentViewController(atIndex: index - 1)
    }
}

//MARK: - UIPageViewControllerDelegate
extension SliderController: UIPageViewControllerDelegate {
    
    public func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        
        if !completed {
            return
        }
        
        self.currentIndex = (pageController?.viewControllers?.first as! SliderItemController).index
    }
}

//MARK: - UIScrollViewDelegate
extension SliderController: UIScrollViewDelegate {
    
    public func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {}
    
    public func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {}
}
