//
//  WeatherView.swift
//  SkywellTask
//
//  Created by Maksim Avksentiev on 2/7/19.
//  Copyright © 2019 Avksentiev. All rights reserved.
//

import UIKit

class WeatherView: SBaseView {

    override var className: String {
        return "WeatherView"
    }
    
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var weatherLabel: UILabel!
    @IBOutlet weak var temperatureLabel: UILabel!
    @IBOutlet weak var weatherState: UIImageView!
    @IBOutlet weak var dashedLine: DashedLine!
    @IBOutlet weak var backgroundImage: UIImageView!
    
    //MARK: - LifeCycle
    override func awakeFromNib() {
        
        super.awakeFromNib()
        
        self.backgroundImage.image = R.image.weatherBg()
        self.backgroundImage.alpha = 0.75
    }
}

//MARK: - Bind
extension WeatherView {
    
    func bind(forecast: Forecast) {
        
        self.weatherLabel.text = forecast.weatherDescription
        self.cityLabel.text = forecast.country + ". " + forecast.name
        self.temperatureLabel.text = String(format: "%.0f", forecast.temp)
    }
}
