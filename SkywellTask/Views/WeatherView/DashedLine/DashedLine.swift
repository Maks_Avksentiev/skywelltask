//
//  DashedLine.swift
//  SkywellTask
//
//  Created by Maksim Avksentiev on 2/7/19.
//  Copyright © 2019 Avksentiev. All rights reserved.
//

import UIKit

@IBDesignable class DashedLine: UIView {
    
    @IBInspectable var perDashLength: CGFloat = 2.0
    @IBInspectable var spaceBetweenDash: CGFloat = 2.0
    @IBInspectable var dashColor = UIColor.lightGray
    
    override func draw(_ rect: CGRect) {
        
        super.draw(rect)
        
        let path = UIBezierPath()
        
        if self.height > self.width {
            
            let p0 = CGPoint(x: self.bounds.midX, y: self.bounds.minY)
            path.move(to: p0)
            
            let p1 = CGPoint(x: self.bounds.midX, y: self.bounds.maxY)
            path.addLine(to: p1)
            path.lineWidth = self.width
        } else {
            
            let p0 = CGPoint(x: self.bounds.minX, y: self.bounds.midY)
            path.move(to: p0)
            
            let p1 = CGPoint(x: self.bounds.maxX, y: self.bounds.midY)
            path.addLine(to: p1)
            path.lineWidth = self.height
        }
        
        let dashes = [self.perDashLength, self.spaceBetweenDash]
        path.setLineDash(dashes, count: dashes.count, phase: 0.0)
        
        path.lineWidth = 5.0
        path.lineCapStyle = .butt
        self.dashColor.set()
        path.stroke()
    }
    
    private var width: CGFloat {
        
        return self.bounds.width
    }
    
    private var height: CGFloat {
        
        return self.bounds.height
    }
}
